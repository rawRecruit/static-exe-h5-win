/*
 * 自适应
 * 获取视口的宽度
 * */
var pageWidth = window.innerWidth,
    pageHeight = window.innerHeigth;
if (typeof pageWidth != "number") {
    if (document.compactMode == "CSS1Compat") {
        pageWidth = document.documentElement.clientWidth;
        pageHeight = document.documentElement.clientHeigth;
    } else {
        pageWidth = document.body.clientWidth;
        pageHeight = document.body.clientHeight;
    }
}
// console.log(pageWidth);
if (pageWidth <= 750) {
	
	var useragent = navigator.userAgent;
	if (useragent.match(/MicroMessenger/i) != 'MicroMessenger') {
		window.location.href = 'https://www.jupaokeji.com/'
	}
	
    (function(doc, win) {
        var docEl = doc.documentElement,
            resizeEvt =
                "orientationchange" in window ? "orientationchange" : "resize",
            pW = 750,
            recalc = function() {
                var cW = docEl.clientWidth;
                if (!cW) {
                    return;
                }
                docEl.style.cssText = "font-size:" + 100 * (cW / pW) + "px !important";
            };
        recalc();
        win.addEventListener(resizeEvt, recalc, false);
    })(document, window);
} else {
    (function(doc, win) {
        var docEl = doc.documentElement,
            resizeEvt =
                "orientationchange" in window ? "orientationchange" : "resize",
            pW = 1200,
            recalc = function() {
                var cW = docEl.clientWidth;
                if (!cW) {
                    return;
                }
                docEl.style.cssText = "font-size:" + 100 + "px !important";
                
            };
        recalc();
        win.addEventListener(resizeEvt, recalc, false);
    })(document, window);
}

